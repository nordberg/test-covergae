'use strict';

var expect = require('expect.js'),
	got = require('got');

describe('Api test', function() {
	it('test with 2', function(done) {
		got('127.0.0.1:3000/2', function(err, body, resp) {
			expect(body).to.eql('foo');
			done();
		});
	});

	it('test with other', function(done) {
		got('127.0.0.1:3000/3', function(err, body, resp) {
			expect(body).to.eql('bar');
			done();
		});
	});
});
