'use strict';

var check = require('../check'),
	expect = require('expect.js');


describe('Check local', function() {
	it('check with small value', function() {
		expect(check(1)).to.be(false);
	});

	it('check with big value', function() {
		expect(check(4)).to.be(true);
	});
});
