'use strict';

var express = require('express'),
	app = express();

app.get('/:param', function (req, res) {
	if (req.params.param == '2') {
		res.send('foo')
	} else {
		res.send('bar')
	}
});

app.listen(3000);
